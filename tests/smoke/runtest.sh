#!/bin/bash
cd ../source
aclocal
autoheader
autoconf
automake --add-missing
export CSCOPE_BINARY=/usr/bin/cscope
./configure && make check
